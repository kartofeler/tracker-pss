package com.andrzej.Tracker.Model;

import android.content.Context;
import com.andrzej.Tracker.Interfaces.*;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDataRepository;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDatabaseHelper;
import com.andrzej.Tracker.Model.Entities.NodesTemplate;
import com.andrzej.Tracker.Model.Entities.TrainingNode;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public class TrainingBuilder {

    private Context context;
    private GpsManager gpsManager;
    private GpsPointsContainer container;
    private RouteSettings routeSettings;
    private MagneticDeviceManager magneticManager;
    private NodeManagerInterface nodeManager;
    private IntervalManagerInterface intervalManager;

    public TrainingBuilder(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public GpsManager getGpsManager() {
        return gpsManager;
    }

    public GpsPointsContainer getContainer() {
        return container;
    }

   public RouteSettings getRouteSettings() {
        return routeSettings;
    }

    public MagneticDeviceManager getMagneticManager() {
        return magneticManager;
    }

    public NodeManagerInterface getNodeManager() {
        return nodeManager;
    }

    public IntervalManagerInterface getIntervalManager() {
        return intervalManager;
    }

    public TrainingBuilder setGpsManager(GpsManager gpsManager) {
        this.gpsManager = gpsManager;
        return this;
    }

    public void setRouteSettings(RouteSettings settings) {
        this.routeSettings = settings;
        setMagneticManager(new BaseMagneticManager(context));
        if(routeSettings.isGpsOn()){
            setGpsManager(new GpsManager(context));
            setGpsPointsContainer(new DefaultGpsPointsContainer());
        }
        if(routeSettings.getSelectedNodeTemplate() != -1){
            TrackerDataRepository helper = new TrackerDatabaseHelper(context);
            NodesTemplate template = helper.getNodesTemplateById(RouteSettings
                    .getInstance()
                    .getSelectedNodeTemplate());

            ArrayList<TrainingNode> nodes;

            try {
                nodes = (ArrayList)helper.getNodesForTemplate(template);
                setNodeManager(new BaseNodeManager(nodes));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private TrainingBuilder setContainer(GpsPointsContainer container) {
        this.container = container;
        return this;
    }

    private TrainingBuilder setGpsPointsContainer(GpsPointsContainer container) {
        this.container = container;
        return this;
    }

    private TrainingBuilder setContext(Context context) {
        this.context = context;
        return this;
    }

    private TrainingBuilder setMagneticManager(MagneticDeviceManager magneticManager) {
        this.magneticManager = magneticManager;
        return this;
    }

    private TrainingBuilder setNodeManager(NodeManagerInterface nodeManager) {
        this.nodeManager = nodeManager;
        return this;
    }

    private TrainingBuilder setIntervalManager(IntervalManagerInterface intervalManager) {
        this.intervalManager = intervalManager;
        return this;
    }

    public TrainingModel build() {
        return new BaseTraining(this);
    }
}
