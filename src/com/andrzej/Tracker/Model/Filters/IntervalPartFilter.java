package com.andrzej.Tracker.Model.Filters;

import com.andrzej.Tracker.Model.Entities.Interval;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-08-09.
 */
public interface IntervalPartFilter {
    public List<Interval> sort(List<Interval> list);
}
