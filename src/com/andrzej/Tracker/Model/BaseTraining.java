package com.andrzej.Tracker.Model;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import com.andrzej.Tracker.Interfaces.*;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDataRepository;
import com.andrzej.Tracker.Model.Entities.*;

import java.util.*;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public class BaseTraining implements TrainingModel, GpsStatusListener, MagneticDeviceListener{

    private ArrayList<TrainingModelListener> modelListeners = new ArrayList<TrainingModelListener>();

    private Context context;
    private GpsManager gpsManager;
    private GpsPointsContainer container;
    private RouteSettings routeSettings;
    private MagneticDeviceManager magneticManager;
    private NodeManagerInterface nodeManager;
    private IntervalManagerInterface intervalManager;

    private Timer trainingTimer;
    private Handler handler;

    private boolean isRunning = false;

    private int secondsCounter;

    public BaseTraining(TrainingBuilder builder) {
        context = builder.getContext();
        gpsManager = builder.getGpsManager();
        container = builder.getContainer();
        routeSettings = builder.getRouteSettings();
        magneticManager = builder.getMagneticManager();
        nodeManager = builder.getNodeManager();
        intervalManager = builder.getIntervalManager();

        trainingTimer = new Timer();
        handler = new Handler();
    }

    /**
     * Metoda służąca do rejestrowania obiektów nasłuchujących na tę klasę
     * @param listener
     */
    @Override
    public void addTrainingModelListener(TrainingModelListener listener) {
        if (!modelListeners.contains(listener)) {
            modelListeners.add(listener);
        }
    }

    /**
     * Służy do wyrejestrowywania obiektów nasłuchujących
     * @param listener
     */
    @Override
    public void removeTrainingModelListener(TrainingModelListener listener) {
        modelListeners.remove(listener);
    }

    /**
     * Rozpoczęcie treningu.
     * Powoduje włączenie GPSa, jeśli jest skonfigurowany, czujnika magnetycznego, jeśli jest dostępny,
     * a także głównego timera treningu
     */
    @Override
    public void startTraining() {
        if(gpsManager != null) {
            gpsManager.addGpsStatusObserver(this);
            gpsManager.startGps();
        }
        if(magneticManager != null) {
            magneticManager.addMagneticInfoListener(this);
            magneticManager.startMagneticDevice();
        }
        if(nodeManager != null){
            //nodeChanges();
        }

        trainingTimer = new Timer();

        trainingTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        secondsCounter++;
                        timeChanged(secondsCounter);
                    }
                });
            }
        }, 500, 1000);

        isRunning = true;
    }

    /**
     * Zatrzymanie treningu, tj. GPSa oraz czujnika magnetycznego, oraz wstrzymanie timera treningu
     */
    @Override
    public void stopTraining() {
        if (gpsManager != null){
            gpsManager.removeGpsStatusObserver(this);
            gpsManager.stopGps();
        }
        if(magneticManager != null) {
            magneticManager.removeMagneticInfoListener(this);
            magneticManager.stopMagneticDevice();
        }
        trainingTimer.cancel();

        isRunning = false;
    }

    /**
     * Zapauzowanie treningu. W zasadzie to samo co metoda służąca do kończenia treningu
     */
    @Override
    public void pauseTraining() {
        if(gpsManager != null) {
            gpsManager.removeGpsStatusObserver(this);
            gpsManager.stopGps();
        }
        if(magneticManager != null) {
            magneticManager.removeMagneticInfoListener(this);
            magneticManager.stopMagneticDevice();
        }
        trainingTimer.cancel();

        isRunning = false;
    }

    /**
     * Sprawdzanie czy trening jest w trakcie trwania
     * @return
     */
    @Override
    public boolean isRunning() {
        return isRunning;
    }

    /**
     * Rozgłoszenie eventu ze zmianą położenia użytkownika
     * @param location
     */
    @Override
    public void locationChanged(MeasureSample location) {
        for (TrainingModelListener listener : modelListeners) {
            listener.locationChangedEvent(secondsCounter, location);
        }
    }

    /**
     * Rozgłoszenie eventu ze zmianą orientacji i odległości od docelowego punktu
     * @param zAxisValue
     * @param distance
     */
    @Override
    public void directionChanged(float zAxisValue, float distance) {
        for (TrainingModelListener listener : modelListeners) {
            listener.directionChangedEvent(zAxisValue, distance);
        }
    }

    /**
     * Rozgłoszenie eventu o zmianie orientacji użytkownika względem północy
     * @param zAxis
     */
    @Override
    public void northChanged(float zAxis) {
        for (TrainingModelListener listener : modelListeners) {
            listener.northChangedEvent(zAxis);
        }
    }

    /**
     * Rozgłoszenie eventu o zmianie aktualnego punktu celu (TrainingNode)
     * @param nodeIndex
     * @param nodeCount
     * @param nodes
     */
    @Override
    public void nodeChanged(int nodeIndex, int nodeCount, TrainingNode[] nodes) {
        for (TrainingModelListener listener : modelListeners){
            listener.nodeChangedEvent(nodeIndex, nodeCount, nodes);
        }
    }

    /**
     * Rozgłoszenie eventu o zmianie czasu trwania treningu
     * @param seconds
     */
    @Override
    public void timeChanged(int seconds) {
        for (TrainingModelListener listener : modelListeners){
            listener.timeChangedEvent(seconds);
        }
    }

    @Override
    public void gpsStatusChanged(float distance) {
        for (TrainingModelListener listener : modelListeners){
            listener.gpsStatusChanged(distance);
        }
    }

    /**
     * Pobranie ostatniego zarejestrowanego położenia wraz z parametrami MeasureSample
     * @return
     */
    @Override
    public MeasureSample getLastLocationSample() {
        if(container != null) {
            return container.getMeasureSample();
        }
        else return null;
    }

    /**
     * Pobranie ostatniego czasu treningu
     * @return
     */
    @Override
    public int getLastTime() {
        return secondsCounter;
    }

    /**
     * Aktualizacja punktów celu w interfejsie użytkownika
     */
    @Override
    public void updateNodesToGui() {
        if(nodeManager != null) {
            nodeChanges();
        }
    }

    /**
     * Metoda dostępowa do pobrania wyszystkich punktów celu danego treningu
     * @return
     */
    @Override
    public List<TrainingNode> getAllNodes() {
        if(nodeManager != null){
            return nodeManager.getAllNodes();
        }
        else{
            return new ArrayList<TrainingNode>();
        }
    }

    /**
     * Pobranie indeksu aktualnego punktu celu
     * @return
     */
    @Override
    public int getCurrentNodeIndex() {
        if(nodeManager != null){
            return nodeManager.getCurrentNodeIndex();
        }
        return 0;
    }

    /**
     * Pobranie całej zarejestrowanej trasy
     * @return
     */
    @Override
    public List<Location> getAllLocation() {
        if(container != null){
            return container.getAllPoints();
        }
        return new ArrayList<Location>();
    }

    /**
     * Zapis treningu do bazy danych
     * @param backendAccess
     * @throws Exception
     */
    @Override
    public void saveTrainingState(TrackerDataRepository backendAccess) throws Exception{
        Statistic statistic = new Statistic();
        if(container != null) {
            MeasureSample lastSample = container.getMeasureSample();
            float pace = (float)(60/(lastSample.meanVelocity * 3.6));
            statistic.setAvgPace( pace );
            float paceMax = (float)(60/(lastSample.maxVelocity * 3.6));
            statistic.setMaxPace( paceMax );
            statistic.setAvgSpeed( (float)lastSample.meanVelocity);
            statistic.setMaxSpeed( (float)lastSample.maxVelocity);
            statistic.setMaxAltitude( (float)lastSample.maxAltitude);
            statistic.setMinAltitude( (float)lastSample.minAltitude);
            statistic.setMaxSlope(0);
            statistic.setTotalDistance((float)lastSample.totalDistance);
            statistic.setTotalTime(secondsCounter);
        }

        backendAccess.addStatistic(statistic);

        Training training = new Training();
        training.setType(routeSettings.getType());
        training.setDate(new Date());
        training.setName(routeSettings.getRouteName());
        training.setNodes_count(nodeManager != null ? nodeManager.getNodesCount() : 0);

        NodesTemplate template = backendAccess.getNodesTemplateById(routeSettings.getSelectedNodeTemplate());
        training.setOrientation_route_name(template != null ? template.getName() : "");
        training.setStatistic(statistic);


        backendAccess.addTraining(training);

        List<GpsSample> gpsSamples = new ArrayList<GpsSample>();
        if(container!= null){
            List<Location> allPoints = container.getAllPoints();
            for(Location point : allPoints){
                GpsSample sample = new GpsSample((float)point.getLatitude(), (float)point.getLongitude(),
                                                (float)point.getAltitude(), point.getSpeed(), training);
                gpsSamples.add(sample);
            }
        }

        backendAccess.addListGpsSample(gpsSamples);
    }


    /**
     * Obsługa eventu zmiany liczby satelit (src GpsManager)
     * @param count
     */
    @Override
    public void OnSatellitesCountChanged(int count) {

    }

    /**
     * Obsługa eventu pojawienia się pierwszego fixa (src GpsManager)
     */
    @Override
    public void OnFirstFix() {

    }

    /**
     * Obsługa eventu zmiany dokładnośći odczytu lokacji (src GpsManager)
     * @param accuracy
     */
    @Override
    public void OnAccuracyChanged(float accuracy) {

    }

    /**
     * Obsługa eventu zmiany lokacji (src GpsManager)
     * @param location
     */
    @Override
    public void OnLocationChanged(Location location) {
        // Jeśli istnieje kontener próbek to zapisuje się otrzymany punkt, a następnie rozgłasza event
        if(container != null) {
            container.addGpsPoint(location);
            MeasureSample sample = container.getMeasureSample();
            sample.meanVelocity = sample.totalDistance / secondsCounter;
            locationChanged(sample);
        }
        // Jeśli istnieje manager celów trasy, to sprawdzenie czy użytkownik znajduje się w strefie
        // danego punktu. Jeśli tak to następuje przełączenie na kolejny cel i rozgłoszenie eventu
        if(nodeManager != null){
            boolean canSwitchNode = nodeManager.checkSwitchPossible(location);
            if(canSwitchNode){
                nodeManager.switchToNextNode();
                nodeChanges();
            }
        }
    }

    /**
     * Obsługa eventu zmiany orientacji i dokładności (src MagneticManager)
     * @param values
     * @param accuracy
     */
    @Override
    public void OnMagneticOrientationChanged(float[] values, int accuracy) {
        if(magneticManager != null){
            // Sprawdzenie czy szukana jest północ czy azymut na kolejny punkt celu
            if(checkCompassMode()){
                // TODO: Null pointer gdy wyłączony GPS jest i w containerze nic
                Location curr = container.getLastLocation();
                if(curr == null){
                    northChanged(values[0]);
                }
                else {
                    Location target = nodeManager.getCurrentNode().getLocation();
                    float distance = target.distanceTo(curr);
                    float azimuth = countAsimuth(curr, target);
                    directionChanged(values[0] - azimuth, distance);
                }
            }else{
                northChanged(values[0]);
            }
        }
    }

    /**
     * Sprawdzenie, czy czujnik działa w trybie kompasu czy w trybie azymutu
     * @return
     */
    private boolean checkCompassMode(){
        if(nodeManager == null) return false;
        if(nodeManager.getNodesCount() == 0) return false;
        if(container == null) return true;

        return true;
    }

    /**
     * Zmiana noda i rozgłoszenie obecnego stanu
     */
    private void nodeChanges() {

        TrainingNode[] nodes = new TrainingNode[3];
        nodes[0] = nodeManager.getPreviousNode();
        nodes[1] = nodeManager.getCurrentNode();
        nodes[2] = nodeManager.getNextNode();

        int curr = nodeManager.getCurrentNodeIndex();
        int all = nodeManager.getNodesCount();

        nodeChanged(curr, all, nodes);
    }

    /**
     * Obliczenie azymutu i przeliczenie go na kąt
     * @param current
     * @param target
     * @return
     */
    private float countAsimuth(Location current, Location target){
        double dx = target.getLatitude() - current.getLatitude();
        double dy = target.getLongitude() - current.getLongitude();

        double alfa = Math.atan(dy / dx) * 57.295779;
        if( dx < 0 ) alfa += 200;
        if( dx > 0 && dy < 0) alfa += 400;

        return (float)alfa;
    }
}
