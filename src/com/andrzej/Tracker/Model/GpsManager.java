package com.andrzej.Tracker.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import com.andrzej.Tracker.Interfaces.GpsStatusListener;
import com.andrzej.Tracker.Interfaces.GpsStatusSubject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Andrzej Laptop on 2014-05-07.
 */
public class GpsManager implements GpsStatus.Listener, GpsStatus.NmeaListener, LocationListener, GpsStatusSubject {
    private LocationManager locationManager;
    private Location lastLocation;
    private GpsStatus lastStatus;
    private Context context;
    private boolean isRun = false;

    private Timer gpsTimer;
    private Handler handler;

    private ArrayList<GpsStatusListener> observers = new ArrayList<GpsStatusListener>();

    public GpsManager(Context _context) {
        this.context = _context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        gpsTimer = new Timer();
        handler = new Handler();
    }

    public void searchForSattelites() {
        if (locationManager != null) {
            if (!isRun) {
                locationManager.addGpsStatusListener(this);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                isRun = true;
            }
        }
    }

    public void startGps() {
        gpsTimer = new Timer();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int syncConnPref = Integer.parseInt(sharedPref.getString("gps_time", "3"));

        gpsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, GpsManager.this , null);
                    }
                });

            }
        }, 100, syncConnPref*1000);
    }

    public void stopGps() {
        locationManager.removeGpsStatusListener(this);
        locationManager.removeUpdates(this);

        isRun = false;
    }

    @Override
    public void onGpsStatusChanged(int event) {
        if (lastStatus == null) {
            lastStatus = locationManager.getGpsStatus(null);
        } else {
            lastStatus = locationManager.getGpsStatus(lastStatus);
        }

        if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
            Iterable<GpsSatellite> satellites = lastStatus.getSatellites();
            int count = 0;
            Iterator<GpsSatellite> satIter = satellites.iterator();
            while (satIter.hasNext()) {
                satIter.next();
                count++;
            }
            satellitesCountChanged(count);
        }
        if (event == GpsStatus.GPS_EVENT_FIRST_FIX) {
            firstFix();
        }
    }

    @Override
    public void onNmeaReceived(long l, String s) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (lastLocation == null) lastLocation = location;
        else {
            if (lastLocation.getAccuracy() != location.getAccuracy()) {
                float acc = location.getAccuracy();
                accuracyChanged(acc);
            }
        }
        locationChanged(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void satellitesCountChanged(int count) {
        for (GpsStatusListener listener : observers) {
            listener.OnSatellitesCountChanged(count);
        }
    }

    @Override
    public void firstFix() {
        for (GpsStatusListener listener : observers) {
            listener.OnFirstFix();
        }
    }

    @Override
    public void accuracyChanged(float accuracy) {
        for (GpsStatusListener listener : observers) {
            listener.OnAccuracyChanged(accuracy);
        }
    }

    @Override
    public void locationChanged(Location location) {
        for (GpsStatusListener listener : observers) {
            listener.OnLocationChanged(location);
        }
    }

    @Override
    public void addGpsStatusObserver(GpsStatusListener listener) {
        if (!observers.contains(listener)) {
            observers.add(listener);
        }
    }

    @Override
    public void removeGpsStatusObserver(GpsStatusListener listener) {
        if (observers.contains(listener)) {
            observers.remove(listener);
        }
    }
}
