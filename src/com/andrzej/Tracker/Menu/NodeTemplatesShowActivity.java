package com.andrzej.Tracker.Menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import com.andrzej.Tracker.VibrateUtils;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDataRepository;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDatabaseHelper;
import com.andrzej.Tracker.Model.Entities.NodesTemplate;
import com.andrzej.Tracker.Model.Entities.TrainingNode;
import com.andrzej.Tracker.R;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-31.
 */
public class NodeTemplatesShowActivity extends OrmLiteBaseActivity<TrackerDatabaseHelper> {

    protected TrackerDataRepository helper;
    protected BaseAdapter adapter;

    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_node_editor);

        helper = getHelper();

        updateListView();

        final ListView listViewTemplates = (ListView)findViewById(R.id.lvTemplates);

        listViewTemplates.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                NodesTemplate selectedTemplate = (NodesTemplate)adapter.getItem(i);
                Intent intent = new Intent(NodeTemplatesShowActivity.this, NodeTemplateDetailActivity.class);
                intent.putExtra("templateId", selectedTemplate.getId());
                startActivity(intent);

            }
        });

        listViewTemplates.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int j, long l) {
                final NodesTemplate selectedTemplate = (NodesTemplate) adapter.getItem(j);
                AlertDialog.Builder confirmationWindow = new AlertDialog.Builder(NodeTemplatesShowActivity.this);
                confirmationWindow.setMessage("Czy na pewno chcesz usunąć tę trasę?")
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                helper.removeNodesTemplate(selectedTemplate);

                                updateListView();
                            }
                        })
                        .setNegativeButton("Nie", null)
                        .show();

                return false;
            }
        });

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                VibrateUtils.VibrateShort(NodeTemplatesShowActivity.this);
            }
        });

        ImageButton addButton = (ImageButton)findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setNewTemplate = new Intent(NodeTemplatesShowActivity.this, NodeTemplateAddActivity.class );
                NodeTemplatesShowActivity.this.startActivity(setNewTemplate);
                VibrateUtils.VibrateShort(NodeTemplatesShowActivity.this);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateListView();
    }

    private void updateListView(){
        List<NodesTemplate> templates = helper.getAllNodesTemplate();

        final ListView listViewTemplates = (ListView)findViewById(R.id.lvTemplates);
        adapter = new NodesTemplatesAdapter(templates);
        listViewTemplates.setAdapter(adapter);
    }


    private class NodesTemplatesAdapter extends BaseAdapter{

        private List<NodesTemplate> nodesTemplates;

        private NodesTemplatesAdapter(List<NodesTemplate> nodesTemplates) {
            this.nodesTemplates = nodesTemplates;
        }

        @Override
        public int getCount() {
            return nodesTemplates.size();
        }

        @Override
        public Object getItem(int i) {
            return nodesTemplates.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = NodeTemplatesShowActivity.this.getLayoutInflater();
            View convertView = layoutInflater.inflate(R.layout.item_template, viewGroup, false);

            NodesTemplate template = nodesTemplates.get(i);

            TextView first = (TextView) convertView.findViewById(R.id.firstLine);
            TextView second = (TextView) convertView.findViewById(R.id.secondLine);
            TextView number = (TextView) convertView.findViewById(R.id.tvNumber);

            first.setText(template.getName());
            second.setText(template.getDescription());
            List<TrainingNode> nodesForTemplate;
            try {
                nodesForTemplate = helper.getNodesForTemplate(template);
                number.setText(String.valueOf(nodesForTemplate.size()));
            } catch (SQLException e) {
                number.setText("-");
            }

            return convertView;
        }
    }
}