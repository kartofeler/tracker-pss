package com.andrzej.Tracker.Menu;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDatabaseHelper;
import com.andrzej.Tracker.Model.Entities.NodeForTemplateJoin;
import com.andrzej.Tracker.Model.Entities.NodesTemplate;
import com.andrzej.Tracker.Model.Entities.TrainingNode;
import com.andrzej.Tracker.R;
import com.andrzej.Tracker.VibrateUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-31.
 */
public class NodeTemplateAddActivity extends OrmLiteBaseActivity<TrackerDatabaseHelper> {

    private GoogleMap map;
    private List<LatLng> selectedNodes = new ArrayList<LatLng>();
    private float distance = 0;
    private int locationCounts = 0;

    private TrackerDatabaseHelper helper;

    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_add_node_template);


        helper = getHelper();
        selectedNodes.clear();

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();


        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        if(lastKnownLocation != null) {
            LatLng last = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(last, 16));
        }

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                VibrateUtils.VibrateShort(NodeTemplateAddActivity.this);
                locationCounts++;
                selectedNodes.add(latLng);
                map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(String.valueOf(locationCounts)));
                new AsyncTask<Void,Void,Void>(){
                    @Override
                    protected Void doInBackground(Void... voids) {
                        distance = 0;
                        for(int i = 1; i < locationCounts; i++){
                            LatLng prev = selectedNodes.get(i-1);
                            LatLng curr = selectedNodes.get(i);

                            Location locCurr = new Location("");
                            locCurr.setLatitude(curr.latitude);
                            locCurr.setLongitude(curr.longitude);

                            Location locPrev = new Location("");
                            locPrev.setLatitude(prev.latitude);
                            locPrev.setLongitude(prev.longitude);

                            float deltaDistance = locCurr.distanceTo(locPrev);
                            distance += deltaDistance;
                        }

                        updateValuesOnBottom();

                        return null;
                    }
                }.execute();
            }
        });

        ImageButton clearAllButton = (ImageButton)findViewById(R.id.remove_button);
        clearAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(NodeTemplateAddActivity.this);
                map.clear();
                selectedNodes.clear();
                locationCounts = 0;
                distance = 0;
                updateValuesOnBottom();
            }
        });

        ImageButton saveButton = (ImageButton)findViewById(R.id.add_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(NodeTemplateAddActivity.this);
                EditText nameEdit = (EditText)findViewById(R.id.etTemplateName);
                String name = nameEdit.getText().toString();

                DecimalFormat formatter = new DecimalFormat("0.00km");
                NodesTemplate template = new NodesTemplate(name, formatter.format(distance/1000));

                helper.addNodesTemplate(template);


                for(LatLng location : selectedNodes){
                    TrainingNode node = new TrainingNode((float)location.latitude, (float)location.longitude);
                    helper.addNode(node);
                    NodeForTemplateJoin join = new NodeForTemplateJoin(template, node);
                    helper.addNodeToTemplate(join);
                }

                NodeTemplateAddActivity.this.onBackPressed();
            }
        });

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VibrateUtils.VibrateShort(NodeTemplateAddActivity.this);
                onBackPressed();
            }
        });
    }

    private void updateValuesOnBottom() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tvDistance = (TextView)findViewById(R.id.tvDistance);
                DecimalFormat formatter = new DecimalFormat("0.00km");

                TextView tvPoints = (TextView)findViewById(R.id.tvCount);

                tvDistance.setText("Dystans:" + formatter.format(distance/1000));
                tvPoints.setText("Punktów:" + String.valueOf(locationCounts));
            }
        });
    }
}
