package com.andrzej.Tracker.Menu;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.andrzej.Tracker.R;

/**
 * Created by Andrzej Laptop on 2014-05-02.
 */
public class ConfigureActivity extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.app_preference);
    }
}