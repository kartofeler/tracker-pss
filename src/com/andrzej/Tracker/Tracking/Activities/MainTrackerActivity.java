package com.andrzej.Tracker.Tracking.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Window;
import com.andrzej.Tracker.Interfaces.*;
import com.andrzej.Tracker.Model.Entities.TrainingNode;
import com.andrzej.Tracker.Model.MeasureSample;
import com.andrzej.Tracker.Model.RouteSettings;
import com.andrzej.Tracker.Model.SpecializedProgressArchiver;
import com.andrzej.Tracker.Model.TrainingBuilder;
import com.andrzej.Tracker.R;

/**
 * Created by Andrzej Laptop on 2014-05-02.
 */
public class MainTrackerActivity extends FragmentActivity implements TrainingModelListener {

    private TrainingProgressKeeper archive;

    private ViewPager viewPager;
    private TrackerPagerAdapter mAdapter;

    private TrainingModel currentTraining;

    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_main_tracker);

        TrainingBuilder builder = new TrainingBuilder(this);
        builder.setRouteSettings(RouteSettings.getInstance());
        currentTraining = builder.build();

        currentTraining.addTrainingModelListener(this);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        int syncConnPref = Integer.parseInt(sharedPref.getString("gps_time", "3"));

        archive = new SpecializedProgressArchiver(syncConnPref);
        currentTraining.addTrainingModelListener((TrainingModelListener)archive);

        viewPager = (ViewPager) findViewById(R.id.pager);
        if(RouteSettings.getInstance().isGpsOn()) {
            mAdapter = new NormalTrainingAdapter(getSupportFragmentManager());
        } else{
            mAdapter = new GpsOffTrainingAdapter(getSupportFragmentManager());
        }
        viewPager.setAdapter(mAdapter);

        currentTraining.startTraining();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder confirmationWindow = new AlertDialog.Builder(this);
        confirmationWindow.setMessage("Czy na pewno chcesz porzucić trening?")
                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainTrackerActivity.this.currentTraining.removeTrainingModelListener(MainTrackerActivity.this);
                        MainTrackerActivity.this.currentTraining.stopTraining();
                        MainTrackerActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("Nie", null)
                .show();
    }

    @Override
    protected void onDestroy() {
        currentTraining.removeTrainingModelListener(this);
        currentTraining.stopTraining();
        super.onDestroy();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void locationChangedEvent(int time, MeasureSample measureSample) {
        int i = viewPager.getCurrentItem();

        Fragment frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i);
        if (frag instanceof MeasureScreen) {
            ((MeasureScreen) frag).newLocation(time, measureSample);
        }
        if(frag instanceof MapScreen){
            ((MapScreen) frag).updateRoute();
        }
    }

    @Override
    public void directionChangedEvent(float zAxis, float distance) {
        int i = viewPager.getCurrentItem();

        Fragment frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i);
        if (frag instanceof OrientationScreen) {
            ((OrientationScreen) frag).newOrientation(zAxis, distance);
        }
    }

    @Override
    public void northChangedEvent(float zAxis) {
        int i = viewPager.getCurrentItem();

        Fragment frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i);
        if (frag instanceof OrientationScreen) {
            ((OrientationScreen) frag).newNorth(zAxis);
        }
    }

    @Override
    public void nodeChangedEvent(int currentNodeIndex, int nodesCount, TrainingNode[] nodes) {
        int i = viewPager.getCurrentItem();

        Fragment frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i);
        if (frag instanceof OrientationScreen) {
            ((OrientationScreen) frag).updateNodeInformation(currentNodeIndex, nodesCount, nodes);
        }

        frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i+1);
        if(frag != null) {
            if (frag instanceof OrientationScreen) {
                ((OrientationScreen) frag).updateNodeInformation(currentNodeIndex, nodesCount, nodes);
            }
        }

        frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i-1);
        if(frag != null) {
            if (frag instanceof OrientationScreen) {
                ((OrientationScreen) frag).updateNodeInformation(currentNodeIndex, nodesCount, nodes);
            }
        }
    }

    @Override
    public void timeChangedEvent(int seconds) {
        int i = viewPager.getCurrentItem();

        Fragment frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i);
        if (frag instanceof MeasureScreen) {
            ((MeasureScreen) frag).newTime(seconds);
        }
    }

    @Override
    public void gpsStatusChanged(float distance) {
        int i = viewPager.getCurrentItem();

        Fragment frag = ((TrackerPagerAdapter) viewPager.getAdapter()).getItem(i);
        if (frag instanceof GpsScreen) {
            ((GpsScreen) frag).gpsStatusChange(distance);
        }
    }


    public TrainingModel getCurrentTraining() {
        return currentTraining;
    }

    public TrainingProgressKeeper getArchive() {
        return archive;
    }

}

