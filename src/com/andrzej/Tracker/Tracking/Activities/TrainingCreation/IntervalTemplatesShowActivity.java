package com.andrzej.Tracker.Tracking.Activities.TrainingCreation;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDatabaseHelper;
import com.andrzej.Tracker.Model.Entities.Interval;
import com.andrzej.Tracker.Model.Entities.IntervalTemplate;
import com.andrzej.Tracker.R;
import com.andrzej.Tracker.VibrateUtils;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by The Ja on 2014-05-29.
 */
public class IntervalTemplatesShowActivity extends OrmLiteBaseActivity<TrackerDatabaseHelper> {

    protected TrackerDatabaseHelper helper;
    private BaseAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_add_interval_template);

        helper = getHelper();

        final ListView listViewTemplates = (ListView)findViewById(R.id.lvTemplates);

        listViewTemplates.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                IntervalTemplate selectedTemplate = (IntervalTemplate)adapter.getItem(i);
                //TODO !!!!!!!!!
                Intent intent = new Intent(IntervalTemplatesShowActivity.this, IntervalTemplateDetailsActivity.class);
                intent.putExtra("templateId", selectedTemplate.getId());
                startActivity(intent);

            }
        });

        listViewTemplates.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int j, long l) {
                final IntervalTemplate selectedTemplate = (IntervalTemplate) adapter.getItem(j);
                AlertDialog.Builder confirmationWindow = new AlertDialog.Builder(IntervalTemplatesShowActivity.this);
                confirmationWindow.setMessage("Czy na pewno chcesz usunąć tę trasę?")
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                helper.removeIntervalTemplate(selectedTemplate);
                                updateListView();
                            }
                        })
                        .setNegativeButton("Nie", null)
                        .show();

                return false;
            }
        });

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                VibrateUtils.VibrateShort(IntervalTemplatesShowActivity.this);
            }
        });

        ImageButton addButton = (ImageButton)findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setNewTemplate = new Intent(IntervalTemplatesShowActivity.this, IntervalTemplateAddActivity.class );
                IntervalTemplatesShowActivity.this.startActivity(setNewTemplate);
                VibrateUtils.VibrateShort(IntervalTemplatesShowActivity.this);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateListView();
    }

    private void updateListView(){
        List<IntervalTemplate> templates = helper.getAllIntervalTemplate();

        final ListView listViewTemplates = (ListView)findViewById(R.id.lvTemplates);
        adapter = new IntervalTemplatesAdapter(templates);
        listViewTemplates.setAdapter(adapter);
    }

    private class IntervalTemplatesAdapter extends BaseAdapter {

        private List<IntervalTemplate> nodesTemplates;

        private IntervalTemplatesAdapter(List<IntervalTemplate> nodesTemplates) {
            this.nodesTemplates = nodesTemplates;
        }

        @Override
        public int getCount() {
            return nodesTemplates.size();
        }

        @Override
        public Object getItem(int i) {
            return nodesTemplates.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = IntervalTemplatesShowActivity.this.getLayoutInflater();
            View convertView = layoutInflater.inflate(R.layout.item_template, viewGroup, false);

            IntervalTemplate template = nodesTemplates.get(i);

            TextView first = (TextView) convertView.findViewById(R.id.firstLine);
            TextView second = (TextView) convertView.findViewById(R.id.secondLine);
            TextView number = (TextView) convertView.findViewById(R.id.tvNumber);

            first.setText(template.getName());
            second.setVisibility(View.GONE);
            List<Interval> intervalsList = null;
            try {
                intervalsList = helper.getIntervalsForTemplate(template);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            number.setText(String.valueOf(intervalsList.size()));

            return convertView;
        }
    }

}
