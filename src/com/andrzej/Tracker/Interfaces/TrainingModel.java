package com.andrzej.Tracker.Interfaces;

import android.location.Location;
import com.andrzej.Tracker.Model.DatabaseAccess.TrackerDataRepository;
import com.andrzej.Tracker.Model.MeasureSample;
import com.andrzej.Tracker.Model.Entities.TrainingNode;

import java.util.List;

/**
 * Created by Andrzej Laptop on 2014-05-11.
 */
public interface TrainingModel{
    public void addTrainingModelListener(TrainingModelListener listener);
    public void removeTrainingModelListener(TrainingModelListener listener);
    public void startTraining();
    public void stopTraining();
    public void pauseTraining();

    public boolean isRunning();

    public void locationChanged(MeasureSample location);
    public void directionChanged(float zAxisValue, float distance);
    public void northChanged(float zAxis);
    public void nodeChanged(int nodeIndex, int nodeCount, TrainingNode[] nodes);
    public void timeChanged(int seconds);
    public void gpsStatusChanged(float distance);

    // Część interfejsu stanowiąca fasadę

    public MeasureSample getLastLocationSample();
    public int getLastTime();
    public void updateNodesToGui();

    public List<TrainingNode> getAllNodes();
    public int getCurrentNodeIndex();

    public List<Location> getAllLocation();

    public void saveTrainingState(TrackerDataRepository backendAccess) throws Exception;
}
