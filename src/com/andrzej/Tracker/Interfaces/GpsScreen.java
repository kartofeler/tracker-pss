package com.andrzej.Tracker.Interfaces;

/**
 * Created by Andrzej Laptop
 */
public interface GpsScreen {
    public void gpsStatusChange(float distance);
}
