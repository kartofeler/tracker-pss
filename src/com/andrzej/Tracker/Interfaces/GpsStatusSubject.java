package com.andrzej.Tracker.Interfaces;

import android.location.Location;

/**
 * Created by Andrzej Laptop on 2014-05-09.
 */
public interface GpsStatusSubject {
    public void satellitesCountChanged(int count);
    public void firstFix();
    public void accuracyChanged(float accuracy);

    public void locationChanged(Location location);

    public void addGpsStatusObserver(GpsStatusListener listener);
    public void removeGpsStatusObserver(GpsStatusListener listener);
}
