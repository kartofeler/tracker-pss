package com.andrzej.Tracker.Interfaces;

import com.andrzej.Tracker.Model.MeasureSample;

/**
 * Created by Andrzej Laptop on 2014-05-12.
 */
public interface MeasureScreen {
    public void newLocation(int time, MeasureSample sample);
    public void newTime(int seconds);
}
