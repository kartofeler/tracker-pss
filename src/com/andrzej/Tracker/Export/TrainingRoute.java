package com.andrzej.Tracker.Export;

import com.andrzej.Tracker.Model.Entities.GpsSample;

import java.util.List;

/**
 * Created by The Ja on 2014-06-07.
 */
public class TrainingRoute {
    private String Name;
    private List<Placemark> Placemarks;
    private List<GpsSample> Samples;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public List<Placemark> getPlacemarks() {
        return Placemarks;
    }

    public void setPlacemarks(List<Placemark> placemarks) {
        Placemarks = placemarks;
    }

    public List<GpsSample> getSamples() {
        return Samples;
    }

    public void setSamples(List<GpsSample> samples) {
        Samples = samples;
    }
}
