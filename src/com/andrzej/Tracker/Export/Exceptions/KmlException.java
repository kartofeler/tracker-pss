package com.andrzej.Tracker.Export.Exceptions;

/**
 * Created by The Ja on 2014-06-08.
 */
public class KmlException extends ExportException {
    public KmlException(){super();}
    public KmlException(String message){super(message);}
}
