package com.andrzej.Tracker.Export;

import com.andrzej.Tracker.Export.Exceptions.ExportException;

/**
 * Created by The Ja on 2014-06-07.
 */
public interface RouteExporter {

    public void setRoute(TrainingRoute route);
    public TrainingRoute getRoute();

    public String Export()  throws ExportException;
}
