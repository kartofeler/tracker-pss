package com.andrzej.Tracker.Export;

import com.andrzej.Tracker.Model.Entities.GpsSample;

/**
 * Created by The Ja on 2014-06-07.
 */
public class Placemark {

    public Placemark(String name, GpsSample position){
        Position = position;
        Name = name;
    }

    private GpsSample Position;
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public GpsSample getPosition() {
        return Position;
    }

    public void setPosition(GpsSample position) {
        Position = position;
    }
}
